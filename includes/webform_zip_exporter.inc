<?php

/**
 * Class to handle downloading webform submissions as a zip file.
 */
class webform_zip_exporter extends webform_exporter_delimited {

  /**
   * {@inheritdoc}
   */
  public function __construct($options) {
    unset($options['delimiter']);
    parent::__construct($options);
  }

  /**
   * {@inheritdoc}
   */
  function post_process(&$results) {
    $zip = new ZipArchive;
    $zip_uri = _webform_export_tempname();
    $zip->open(drupal_realpath($zip_uri), ZipArchive::CREATE);

    // Add the submission CSV file and all associated attached files.
    $zip->addFile(drupal_realpath($results['file_name']), 'submissions.csv');
    $zip->addEmptyDir('files');
    $submission_files = $this->get_export_files($results['file_name']);
    foreach ($submission_files as $submission_file) {
      $zip->addFile(drupal_realpath($submission_file), 'files/' . drupal_basename($submission_file));
    }
    $zip->close();

    // Replace the results download with the zip file.
    unlink($results['file_name']);
    rename($zip_uri, $results['file_name']);
  }

  /**
   * Get a list of files to add to the zip file.
   *
   * The data to attach to the zip file must come from the export file itself.
   * We can't save any state during the processing of each submission because
   * we don't have any mechanism to get data into the batch sandbox, thus the
   * only available state is the associated file handle.
   *
   * Luckily we have the right amount of metadata at the end of the row
   * handling, namely the CSV column order in combination with the data in
   * webform_zip_exporter_webform_results_download_submission_information_info
   * to extract the file URIs from the final CSV file.
   *
   * @param string $file_uri
   *   The URI of the CSV file containing submission information.
   * @return array
   *   An array of file URIs to add the zip file.
   */
  protected function get_export_files($file_uri) {
    $zip_files = array();
    // How many rows to skip based on header settings.
    $skip_rows = $this->options['header_keys'] == '-1' ? 0 : 3;
    // The key in the submission that has the attached files.
    $files_key = array_search('attached_files', array_values($this->options['components'])) - 1;
    $i = 0;
    $file_handle = fopen($file_uri, 'r');
    while ($submission = fgetcsv($file_handle, 0, $this->delimiter)) {
      if ($i < $skip_rows) {
        $i++;
        continue;
      }
      $zip_files = array_merge($zip_files, array_map('trim', explode(',', $submission[$files_key])));
    }
    return $zip_files;
  }

  /**
   * {@inheritdoc}
   */
  function set_headers($filename) {
    drupal_add_http_header('Content-Disposition', "attachment; filename=$filename.zip");
  }

}
